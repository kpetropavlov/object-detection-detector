FROM jjanzic/docker-python3-opencv

ADD . /app

WORKDIR /app

RUN pip install -r requirements.txt

EXPOSE 5000

CMD [ "python", "./detect.py" ]
