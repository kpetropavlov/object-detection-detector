# Importing packages
from detecto import core, utils, visualize
from flask import Flask, escape, request, jsonify, Response
import skimage
from flask_cors import CORS
import numpy as np
import torch
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import io

# Initilaising app and wrapping it in CORS to allow request from different services
app = Flask(__name__)
CORS(app)

# Telling matplotlib to not create GUI Windows as our application is backend and doesn't require direct visulaization
matplotlib.use('agg')

# Loading our custom model
model = core.Model.load('model_weights.pth', ['pear', 'strawberry'])

# Adding new POST endpoint that will accept image and output image with bounding boxes of detected objects
@app.route("/detect", methods=['POST'])
def detect():
    
    # Accessing file from request
    file = request.files['image']
    image = skimage.io.imread(file)

    # Using model to detect objects
    predictions = model.predict(image)
    labels, boxes, scores = predictions

    # Applying threshold
    lab = []
    box = []
    for i in range(len(scores)):
        if scores[i] > 0.4:
            lab.append(labels[i])
            box.append(boxes[i])

    box = torch.stack(box)

    # Creating figure and displaying original image
    fig,ax = plt.subplots(1)
    ax.imshow(image)

    # Adding bounding boxes
    for i in range(len(box)):
        ax.add_patch(patches.Rectangle((box[i][0],box[i][1]),box[i][2] - box[i][0],box[i][3] - box[i][1],linewidth=1,edgecolor='r',facecolor='none'))
    
    # Preparing output
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    
    # Sending response as png image
    return Response(output.getvalue(), mimetype='image/png')

if __name__ == '__main__':
    app.debug = True
    app.run(host = '0.0.0.0',port=5000, threaded=True)
